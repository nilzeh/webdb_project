-- DROP DATABASE IF EXISTS ProjectTest;
-- CREATE DATABASE ProjectTest;

DROP DATABASE IF EXISTS webdb;
CREATE DATABASE webdb;
USE webdb;

CREATE TABLE IF NOT EXISTS account
(
	email_address CHAR(255) NOT NULL,
	password CHAR(128) NOT NULL,
	salt CHAR(128) NOT NULL,
	nickname CHAR(255) NOT NULL,
	score INT NOT NULL,
	gender ENUM('M', 'F') NOT NULL,
	birthdate DATE NOT NULL,
	PRIMARY KEY(email_address)
);

CREATE TABLE IF NOT EXISTS task
(
	id INT NOT NULL AUTO_INCREMENT,
	name CHAR(255) NOT NULL,
	media_file_name CHAR(255),
	owner CHAR(255) NOT NULL,
	reward INT NOT NULL,
	PRIMARY KEY(id),
	FOREIGN KEY(owner)
		REFERENCES account(email_address)
);

CREATE TABLE IF NOT EXISTS reaction
(
	id INT NOT NULL AUTO_INCREMENT,
	task INT NOT NULL,
	reaction TEXT NOT NULL,
	account CHAR(255) NOT NULL,
	PRIMARY KEY(id),
	FOREIGN KEY(task)
		REFERENCES task(id),
	FOREIGN KEY(account)
		REFERENCES account(email_address)
);

CREATE TABLE IF NOT EXISTS question
(
	id INT NOT NULL AUTO_INCREMENT,
	task INT NOT NULL,
	question_text TEXT NOT NULL,
	PRIMARY KEY(id),
	FOREIGN KEY (task)
		REFERENCES task(id)
);

CREATE TABLE IF NOT EXISTS choice
(
	id INT NOT NULL AUTO_INCREMENT,
	question INT NOT NULL,
	option_text TEXT NOT NULL,
	PRIMARY KEY(id),
	FOREIGN KEY (question)
		REFERENCES question(id)
);

CREATE TABLE IF NOT EXISTS entry
(
	question INT NOT NULL,
	answer CHAR(255) NOT NULL,
	user CHAR(255) NOT NULL,
	PRIMARY KEY(question, user),
	FOREIGN KEY (question)
		REFERENCES question(id),
	FOREIGN KEY (user)
		REFERENCES account(email_address)
);