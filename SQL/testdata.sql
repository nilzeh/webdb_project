USE webdb;

DELETE FROM reaction;
DELETE FROM entry;
DELETE FROM question;
DELETE FROM task;
DELETE FROM choice;
DELETE FROM account;

INSERT INTO account VALUES (
	'test',
	'83851911edfa104546151e86777a5a1ad48cee82e2e4413b5175a19b44c92705a76f849b27fd16b1b068c0f9e9e414c88fa04837d71f739a3b553cdfb3735530',
	'f28c8dd319b6d34583130e293b0e727da417e64c19170358c67d939b7193d02ffa60350a8ebadaddfe1eaace1699e88cf655bba6b684c82f7fff928bdae7b5b9',
	'test', 0, 'M', DATE('1980-03-04'));

INSERT INTO account VALUES (
	'test2',
	'f5c68ce7dbaa145c01eb001f0f85f061adbb9327c9e2fd68d65f96f9f825004259a10f1fa02976f423e39ee532566a7256ddaba6a7fd1743793c05d13285ae32',
	'2f3059886d0a101b35361cb77bf427e9c8c847ea27fdb7490fc24913c3a29d614767ee9b2c1eefdb1a97bd97478ea519ebabe7d35e2b6e38be8cffbd39320868',
	'test2', 0, 'M', DATE('1980-03-04'));

INSERT INTO  `webdb`.`task` (
`id` ,
`name` ,
`media_file_name` ,
`owner` ,
`reward`
)
VALUES (
NULL ,  'Sports',  'sports.jpg',  'test',  '25'
);

SET @lastID = LAST_INSERT_ID();

INSERT INTO  `webdb`.`reaction` (
`task` ,
`reaction` ,
`account`
)
VALUES (
@lastID, 'Wow great task man!',  'test2'
), (
@lastID, 'Thanks! :)',  'test'
);

INSERT INTO  `webdb`.`question` (
`id` ,
`task` ,
`question_text`
)
VALUES (
NULL ,  @lastID,  'Do you like sports?'
), (
NULL ,  @lastID,  'Do you play sports?'
), (
NULL ,  @lastID,  'What is your favorite sport?'
), (
NULL ,  @lastID,  'What is your favorite sports club?'
), (
NULL ,  @lastID,  'Who is your favorite sports player?'
);


INSERT INTO  `webdb`.`task` (
`id` ,
`name` ,
`media_file_name` ,
`owner` ,
`reward`
)
VALUES (
NULL ,  'Food',  'Food.jpg',  'test',  '25'
);

SET @lastID = LAST_INSERT_ID();

INSERT INTO  `webdb`.`question` (
`id` ,
`task` ,
`question_text`
)
VALUES (
NULL ,  @lastID,  'What is your favorite meal of the day?'
), (
NULL ,  @lastID,  'What is your favorite food?'
), (
NULL ,  @lastID,  'What is your favorite place to eat?'
), (
NULL ,  @lastID,  'Who do prefer to eat with?'
), (
NULL ,  @lastID,  'What food do you find disgusting?'
);


INSERT INTO  `webdb`.`task` (
`id` ,
`name` ,
`media_file_name` ,
`owner` ,
`reward`
)
VALUES (
NULL ,  'Coffee',  'Coffee.jpg',  'test',  '15'
);

SET @lastID = LAST_INSERT_ID();

INSERT INTO  `webdb`.`question` (
`id` ,
`task` ,
`question_text`
)
VALUES (
NULL ,  @lastID,  'What is your favourite coffee brand?'
), (
NULL ,  @lastID,  'What is your favourite coffee shop?'
), (
NULL ,  @lastID,  'What is your favourite coffee moment?'
);

SET @lastID = LAST_INSERT_ID();

INSERT INTO  `webdb`.`entry` (
`question` ,
`answer` ,
`user`
)
VALUES (
@lastID,  'DE',  'test2'
), (
@lastID + 1, 'Starbucks',  'test2'
), (
@lastID + 2, 'Late night',  'test2'
);
