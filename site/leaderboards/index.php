<?php 
	session_start();
	include '../functions.php';
	error_reporting(-1);
	require_once('../db_config.php');
	$db = connectDB();
?>
<!DOCTYPE html>
<html>
	<head>
		<title>
			Leaderboards
		</title>
		<link href="../stylesheet.css" rel="stylesheet" type="text/css">
	</head>
	<body>
		<div id="wrapper">
			<h1>
				LEADERBORDS
			</h1>
<?php
	include 'navbar.php';
	echo '<div id="content">';
	$sql = 'SELECT email_address, nickname FROM account';
	echo 'Top 25 <br>';
	echo '
			<table>
			<tr>
				<th>Rank</th>
				<th>User</th>
				<th>Score</th>
			</tr>
			';

	foreach($db->query($sql) as $account)
	{
		$scores[$account['nickname']] = getUserScore($db, $account['email_address']);
	}

	arsort($scores);

	$rank = 1;
	foreach($scores as $name => $score)
	{
		if ($rank > 25)
		{
			break;
		}

		echo '
		<tr>
		<td>' . $rank . '</td>
		<td>' . $name . '</td>
		<td>' . (isset($score) ? $score : 0) . '</td>
		</tr>';
		++$rank;
	}
?>
			</div>
		</div>
	</body>
</html>