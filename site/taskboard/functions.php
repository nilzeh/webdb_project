<?php
function getReward($db, $taskid)
{
	// Get reward from the task
	$sql = 'SELECT reward FROM task WHERE id = ?';
	$statement = $db->prepare($sql);
	$statement->execute(array($taskid));
	return($statement->fetch()['reward']);
}
?>