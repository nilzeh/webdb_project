<?php
	session_start();
	include '../functions.php';
	include 'functions.php';
	requireLogIn();
	require_once '../db_config.php';
	$db = connectDB();
?>
<html>
	<head>
		<title>
			Website
		</title>
		<link href="../stylesheet.css" rel="stylesheet" type="text/css">
	</head>
	<body>
		<div id="wrapper">
			<h1>
				DOING TASK
			</h1>
<?php 
	include 'navbar.php';
	echo '<div id="content">';
	$success = 1;
	$questions_answered = 0;

	// Get total amount of questions from task
	$sql = "SELECT count(id) FROM question WHERE task = ?";
	$statement = $db->prepare($sql);
	$statement->execute(array($_POST['taskid']));
	$questions_amount = $statement->fetch()['count(id)'];

	// Get amount of already answered questions from task
	$sql = "SELECT count(id) FROM entry JOIN question on entry.question=question.id WHERE task = ? AND user = ?";
	$statement = $db->prepare($sql);
	$statement->execute(array($_POST['taskid'], $_SESSION['email_address']));
	$already_answered = $statement->fetch()['count(id)'];

	// Check if already rewarded
	if ($questions_amount == $already_answered)
	{
		$already_rewarded = true;
	}
	else
	{
		$already_rewarded = false;
	}

	// Loop through each answer
	foreach ($_POST as $question => $answer)
	{
		if ($question == "taskid") 
		{
			// All answers are inserted
			break;
		}

		if ($answer != '')
		{	
			$questions_answered++;
			// Insert entry in database
			$sql = 'INSERT INTO entry (question, answer, user) VALUES (?,?,?)';
			$statement = $db->prepare($sql);
			$success = $statement->execute(array($question, $answer, $_SESSION['email_address']));
			if (!$success)
			{
				// Try updating the entry 
				$sql = 'UPDATE entry SET answer = ? WHERE question = ? AND user = ?';
				$statement = $db->prepare($sql);
				$success = $statement->execute(array($answer, $question, $_SESSION['email_address']));
			}
		}
		else
		{
			// Try clearing the entry if it was removed
			$sql = 'DELETE FROM entry WHERE question = ? AND user = ?';
			$statement = $db->prepare($sql);
			$statement->execute(array($question, $_SESSION['email_address']));
		}
	}
	// All questions answered, give reward
	if ($questions_amount == $questions_answered && !$already_rewarded)
	{
		$reward = getReward($db, $_POST['taskid']);

		// Update user score with reward
		$sql = 'UPDATE account SET score = score + ? WHERE email_address = ?';
		$statement = $db->prepare($sql);
		$statement->execute(array($reward, $_SESSION['email_address']));
	}
	if ($questions_amount != $questions_answered && $already_rewarded)
	{
		// Was rewarded but removed question.
		$reward = getReward($db, $_POST['taskid']);

		// Update user score minus reward
		$sql = 'UPDATE account SET score = score - ? WHERE email_address = ?';
		$statement = $db->prepare($sql);
		$statement->execute(array($reward, $_SESSION['email_address']));
	}
	if ($success)
	{
		echo 'Done. <br><br> <a id="button" href="index.php">Return</a>';
	}
	else
	{
		echo 'Something went wrong. <br><br> <a id="button" href="index.php">Return</a>';
	}
?>
			</div>
		</div>
	</body>
</html>