<?php
	session_start();
	include '../functions.php';
	requireLogIn();
	require_once('../db_config.php');
	$db = connectDB();
	error_reporting(-1);
	ini_set("display_errors", 1);
?>
<html>
	<head>
		<title>
			Website
		</title>
		<link href="../stylesheet.css" rel="stylesheet" type="text/css">
	</head>
	<body>
		<div id="wrapper">
			<h1>
				DOING TASK
			</h1>
<?php 
	include 'navbar.php';

	if (isset($_POST['reaction_submit']))
	{
		if ($_POST['reaction'] == '')
		{
			echo 'Empty reaction not inserted';
		} 
		else
		{
			$statement = $db->prepare('INSERT INTO reaction (task, reaction, account) VALUES(?, ?, ?)');
			$is_inserted = $statement->execute(array(
				$_GET['taskid'], nl2br(strip_tags($_POST['reaction'])), $_SESSION['email_address']));
			if (!$is_inserted)
			{
				echo 'Something went wrong when inserting your reaction';
			}
			else
			{
				echo 'Your reaction has been placed!';
			}
		}
	}
	echo '<div id="content">
		<p>';
	$taskid = $_GET['taskid'];
	// Get all questions with specific taskid
	$sql = 'SELECT * FROM question WHERE task = ?';
	$statement = $db->prepare($sql);
	$statement->execute(array($taskid));
	$questions = $statement->fetchAll();
	
	echo '<form name="input" action="processanswer.php" method="post">';
	foreach ($questions as $question)
	{
		// Get amount of already answered questions
		$sql = 'SELECT answer FROM entry WHERE question = ? AND user = ?';
		$statement = $db->prepare($sql);
		$statement->execute(array($question['id'], $_SESSION['email_address']));
		$answer = $statement->fetch()['answer'];
		echo $question['question_text'] . '<br>
			Answer: <input type="text" name="' . $question['id'] . '" value="' . $answer . '"><br>';
	}
	echo '
				<input type="hidden" name="taskid" value="' . $taskid . '">
				<input type="submit" name="submit" value="Submit">
			</form>
		</p>';

	echo '
		<div id="reactions">';
	// Get all reactions and nicknames with specific taskid
	$sql = 'SELECT nickname, reaction FROM reaction join account ON reaction.account = account.email_address WHERE task = ? ORDER BY reaction.id ASC';
	$statement = $db->prepare($sql);
	$statement->execute(array($taskid));
	$reactions = $statement->fetchAll();
	// print_r($reactions);
	foreach ($reactions as $reaction)
	{
		echo '
		<p class="reaction">
			<strong>' . $reaction['nickname'] . '</strong><br>'
			. $reaction['reaction'];
		echo '
		</p>';
		//echo $reaction['account']
	}
	echo '
		Give your own reaction:
		<form action="dotask.php?taskid=' . $_GET['taskid'] . '" method="post">
			<textarea name="reaction" rows="8" cols="50"></textarea><br>
			<input type="submit" name="reaction_submit" value="Give reaction">
		</form>';
?>
			</div>
		</div>
	</body>
</html>