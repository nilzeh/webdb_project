<?php
	session_start();
	include '../functions.php';
	requireLogIn();
	require_once('../db_config.php');
	$db = connectDB();
?>
<!DOCTYPE html>
<html>
	<head>
		<title>
			Tasks
		</title>
		<link href="../stylesheet.css" rel="stylesheet" type="text/css">
	</head>
	<body>
		<div id="wrapper">
			<h1>
				TASKS
			</h1>
<?php
	include 'navbar.php';
	/* Array of tasks */
?>	<div id="content">
		<p>
			Complete a task to get the reward. Be careful about deleting answers because your reward will be withdrawn!
		</p>
		<table border=1>
			<tr>
				<th>Task</th>
				<th>Owner</th>
				<th>Reward</th>
				<th>Answered</th>
			</tr>
<?php
	// Get all tasks
	$tasks = $db->query('SELECT * FROM task');
		foreach($tasks as $task) {
			echo '
				<tr>
					<td><a href="dotask.php?taskid=' . $task['id'] . '">' . $task['name'] . '</a></td>
					<td>' . $task['owner'] . '</td>
					<td>' . $task['reward'] . '</td>
					<td>';
			// Get question amount from specific task
			$sql = "SELECT count(id) FROM question WHERE task = ?";
			$statement = $db->prepare($sql);
			$statement->execute(array($task['id']));
			$questions_amount = $statement->fetch()['count(id)'];

			// Get amount of already answered questions
			$sql = "SELECT count(question) FROM question JOIN entry on question.id=entry.question where user = ? AND task = ?";
			$statement = $db->prepare($sql);
			$statement->execute(array($_SESSION['email_address'], $task['id']));
			$answered_amount = $statement->fetch()['count(question)'];
			echo $answered_amount . ' out of ' . $questions_amount ;
			echo '</td>
				</tr>';
		}
?>
		</table>
	</body>
</html>