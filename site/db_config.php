<?php
	function connectDB()
	{
		$dsn = 'mysql:dbname=webdb;host=localhost';
		$user = 'webdb';
		$pass = 'test';
		$db = new PDO($dsn, $user, $pass);
		// $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		return $db;
	}
?>