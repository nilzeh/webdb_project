<?php
	session_start();
	if (!isset($_SESSION['email_address']))
	{
		echo '
		<div id="navbar">
			<ul>
				<li>
				<a href="leaderboards/index.php">Leaderboards</a> 
				</li>
				<li>
					<a href="user/login.php">Login</a> 
				</li>
				<li>
					<a href="user/register.php">Register</a> 
				</li>
			</ul>
		</div>
		';
	} 
	else
	{
		echo '
		<div id="navbar">
			<ul>
				<li>
				<a href="leaderboards/index.php">Leaderboards</a> 
				</li>
				<li>
					<a href="taskboard/index.php">Taskboard</a> 
				</li>
				<li>
					<a href="my_tasks/index.php">My tasks</a> 
				</li>
				<li>
					<a href="user/logout.php">Log out</a> 
				</li>
			</ul>
		</div>
		';
	}
?>