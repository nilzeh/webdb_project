<?php
function requireLogIn() {
	if (!isset($_SESSION['email_address'])) {
		session_destroy();
		$_SESSION['message'] = 'You need to be logged in to view this page, redirected to login page';
		header('Location: /webdb_project/site/user/login.php', true);
        die();
    }

    if (isset($_SESSION['timestamp']))
    {
    	$timeout = 60 * 60 * 24;
    	$time_elapsed = time() - $_SESSION['timestamp'];
		if ($time_elapsed > $timeout) // session timeout
		{
			session_destroy();
			$_SESSION['message'] = 'You need to be logged in to view this page, redirected to login page';
			header('Location: /webdb_project/site/user/login.php', true);
        	die();
		}
    }

	session_regenerate_id();
    $_SESSION['timestamp'] = time();
}

function getUserScore($db, $user)
{
	$sql = '
	SELECT SUM(reward) AS score
	FROM task 
	WHERE id in
	(
		SELECT t1.task FROM
		(
			SELECT task, count(task) AS c1
			FROM entry JOIN question ON entry.question = question.id 
			WHERE user = ? 
			GROUP BY task
		) AS t1
		JOIN 
		(
			SELECT task, count(id) AS c2
			FROM question 
			GROUP BY task
		) AS t2
		ON t1.task = t2.task
		WHERE c1 = c2
	)';
	$statement = $db->prepare($sql);
	$statement->execute(array($user));
	return $statement->fetch()['score'];
}
?>