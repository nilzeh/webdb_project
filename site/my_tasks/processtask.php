<?php
	session_start();
	include '../functions.php';
	include 'functions.php';
	requireLogIn();
	error_reporting(-1);
	ini_set("display_errors", 1);
	require_once('../db_config.php');
	$db = connectDB();
?>

<html>
	<head>
		<title>
			Website
		</title>
		<link href="../stylesheet.css" rel="stylesheet" type="text/css">
	</head>
	<body>
		<div id="wrapper">
			<h1>
				ADD TASK
			</h1>
<?php 
	include 'navbar.php';
	echo '<div id="content">';
	$taskid = 0;
	
	if (isset($_POST['taskname']) and isset($_POST['submit']))
	{
		if(isset($_POST['filename']))
		{
			$taskid = insertInTask($db, $_POST['taskname'], $_POST['filename'], $_SESSION['email_address']);
		}
		else
		{
			$taskid = insertInTask($db, $_POST['taskname'], '', $_SESSION['email_address']);
		}
	}

	if ($taskid)
	{
		echo 'Added ' . $_POST['taskname'] . '. <br><br> <a href="addquestion.php?taskid=' . $taskid . '">Add a question.</a>';
	}
	else
	{
		echo 'Something went wrong. <br><br> <a href="index.php">Return</a>';
	}
?>
			</div>
		</div>
	</body>
</html>