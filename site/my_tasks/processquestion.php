<?php
	session_start();
	include '../functions.php';
	include 'functions.php';
	requireLogIn();
	error_reporting(-1);
	ini_set("display_errors", 1);
	require_once('../db_config.php');
	$db = connectDB();
	checkUserAllowed($db, $_SESSION['email_address'], $_GET['taskid']);
?>

<html>
	<head>
		<title>
			Website
		</title>
		<link href="../stylesheet.css" rel="stylesheet" type="text/css">
	</head>
	<body>
		<div id="wrapper">
			<h1>
				ADD TASK
			</h1>
<?php 
	include 'navbar.php';
	echo '<div id="content">';
	$questionid = 0;

	if (isset($_POST['question']) and isset($_GET['taskid']) and isset($_POST['submit']))
	{

	$questionid = insertInQuestion($db, $_GET['taskid'], $_POST['question']);
	}
	if ($questionid)
	{
	echo 'Added \'' . $_POST['question'] . '\'. <br><br> <a id="button" href="addquestion.php?taskid=' . $_GET['taskid'] . '">Add question</a> <a id="button" href="showtask.php?taskid=' . $_GET['taskid'] . '">Done</a>';
	}
	else
	{
	echo 'Something went wrong. <br><br> <a href="tasks.php">Return</a>';
	}
?>
			</div>
		</div>
	</body>
</html>