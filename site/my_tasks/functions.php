<?php
function insertInTask($db, $taskname, $filename, $owner, $reward = 0)
{
	$sql = 'INSERT INTO task (name,media_file_name,owner,reward) VALUES (?,?,?,?)';
	$statement = $db->prepare($sql);
	$statement->execute(array($taskname, strip_tags($filename), $owner, $reward));
	return $db->lastInsertId();
}

function insertInQuestion($db, $taskid, $qtext)
{
	$value = 5; // value of a question
	$sql = 'UPDATE task SET reward = reward + ? WHERE id = ?';
	$statement = $db->prepare($sql);
	$statement->execute(array($value, $taskid));
	$sql = 'INSERT INTO question (task,question_text) VALUES (?,?)';
	$statement = $db->prepare($sql);
	$statement->execute(array($taskid, strip_tags($qtext)));
	return $db->lastInsertId();
}

function checkUserAllowed($db, $user, $taskid)
{
	// Check if someone is allowed to change a thing.
	$sql = 'SELECT owner FROM task WHERE id = ?';
	$statement = $db->prepare($sql);
	$statement->execute(array($taskid));
	$db_user = $statement->fetch()['owner'];
	if ($db_user != $user)
	{
		header('HTTP/1.0 403 Forbidden');
		echo 'You are not allowed to do this!';
		die();
	}
}
?>