<?php
	session_start();
	include '../functions.php';
	include 'functions.php';
	requireLogIn();
	error_reporting(-1);
	ini_set("display_errors", 1);
	require_once('../db_config.php');

	$db = connectDB();
	checkUserAllowed($db, $_SESSION['email_address'], $_GET['taskid']);
	$statement = $db->prepare('SELECT * FROM question WHERE task = ?');
	$statement->execute(array($_GET['taskid']));
	$questions = $statement->fetchAll();
?>

<html>
	<head>
		<title>
			Tasks
		</title>
		<link href="../stylesheet.css" rel="stylesheet" type="text/css">
	</head>
	<body>
		<div id="wrapper">
			<h1>
				TASKS
			</h1>
<?php
	include 'navbar.php';
?>
			<div id="content">
				<form action="showtask.php?taskid=<?=$_GET['taskid']?>" method="post">
<?php
	foreach($questions as $question) {

					echo '
						<input type="text" value="' . $question['question_text'] 
						. '" name="q' . $question['id'] . '" size=102><br>';
	} 
?>
					<input type="submit" name="submit" value="Save">
				</form>
			</div>
		</div>
	</body>
</html>