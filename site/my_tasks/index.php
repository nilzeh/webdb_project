<?php 
	session_start();
	include '../functions.php';
	requireLogIn();
	error_reporting(-1);
	require_once('../db_config.php');
	$db = connectDB();
?>
<!DOCTYPE html>
<html>
	<head>
		<title>
			Tasks
		</title>
		<link href="../stylesheet.css" rel="stylesheet" type="text/css">
	</head>
	<body>
		<div id="wrapper">
			<h1>
				TASKS
			</h1>
<?php
	include 'navbar.php';
?>
			<div id="content">
<?php
	echo '<br>Tasks:<ul>';
	$sql = "SELECT * FROM task WHERE owner = ?";
	$statement = $db->prepare($sql);
	$statement->execute(array($_SESSION['email_address']));
	$tasks = $statement->fetchAll();
	foreach($tasks as $task) {
		echo '
			<li>
				<a href="showtask.php?taskid=' . $task['id'] .'">' . $task['name'] . '</a>
			</li>';
	}
	echo '</ul>';
?>
				<a id="button" href="addtask.php">Create task</a>
			</div>
		</div>
	</body>
</html>