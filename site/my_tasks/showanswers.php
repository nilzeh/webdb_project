<?php
	session_start();
	include '../functions.php';
	include 'functions.php';
	requireLogIn();
	error_reporting(-1);
	ini_set("display_errors", 1);
	require_once('../db_config.php');
	$db = connectDB();
	checkUserAllowed($db, $_SESSION['email_address'], $_GET['taskid']);

	$sql = 'SELECT * FROM entry WHERE question = ?';
	$statement = $db->prepare($sql);
	$statement->execute(array($_GET['qid']));
	$entries = $statement->fetchAll();
?>

<!DOCTYPE html>
<html>
	<head>
		<title>
			Tasks
		</title>
		<link href="../stylesheet.css" rel="stylesheet" type="text/css">
	</head>
	<body>
		<div id="wrapper">
			<h1>
				...
			</h1>
<?php
	include 'navbar.php';
?>
			<div id="content">
				<p>
					Answers 
					<table>
						<tr>
							<th>User</th>
							<th>Answer</th>
						</tr>
<?php
	foreach($entries as $entry) {
		// count the number of answers given to each question
		echo '
			<tr>
				<td>' . $entry['user'] . '</td>
				<td>' . $entry['answer'] . '</td>
			</tr>';
	}
?>
					</table>
					<br>
				</p>
			</div>
		</div>
	</body>
	
</html>