<?php
	session_start();
	include '../functions.php';
	requireLogIn();
?>
<html>
	<head>
		<title>
			Website
		</title>
		<link href="../stylesheet.css" rel="stylesheet" type="text/css">
	</head>
	<body>
		<div id="wrapper">
			<h1>
				ADD TASK
			</h1>
<?php include 'navbar.php' ?>
			<div id="content">
				<form name="input" action="processtask.php" method="post">
					<label for="name">Task name: </label>
					<input id="name" type="text" name="taskname"><br>
					<label for="file">Media file: </label>
					<input id="file" type="file" name="filename"><br>
					<input type="submit" name="submit" value="Submit">
				</form>
			</div>
		</div>
	</body>
</html>