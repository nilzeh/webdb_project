<?php
	session_start();
	include '../functions.php';
	include 'functions.php';
	requireLogIn();
	error_reporting(-1);
	ini_set("display_errors", 1);
	require_once('../db_config.php');
	$db = connectDB();

	checkUserAllowed($db, $_SESSION['email_address'], $_GET['taskid']);
	// if this page is reached via the editing page, update all the edited questions
	if(isset($_POST['submit']) and $_POST['submit'] == "Save")
	{
		foreach ($_POST as $id => $text) {
			if (strpos($id, 'q') === 0)
			{
				$id = ltrim($id, 'q');
				$sql = 'UPDATE question SET question_text = ? WHERE id = ?';
				$statement = $db->prepare($sql);
				$statement->execute(array($text, $id));
			}
		}
	}

	// get the name and reward by taskid
	$statement = $db->prepare('SELECT * FROM task WHERE id = ?');
	$statement->execute(array($_GET['taskid']));
	$task = $statement->fetch();

	// get all the questions belonging to a task
	$statement = $db->prepare('SELECT * FROM question WHERE task = ?');
	$statement->execute(array($_GET['taskid']));
	$questions = $statement->fetchAll();
?>

<!DOCTYPE html>
<html>
	<head>
		<title>
			Tasks
		</title>
		<link href="../stylesheet.css" rel="stylesheet" type="text/css">
	</head>
	<body>
		<div id="wrapper">
			<h1>
				TASKS
			</h1>
<?php
	include 'navbar.php';
?>
			<div id="content">
				<p>
					Name: <?=$task['name']?>
				</p>
				<p>
					Reward: <?=$task['reward']?>
				</p>
				<p>
					Questions 
					<table>
						<tr>
							<th>Text</th>
							<th>#Answers</th>
						</tr>
<?php
	foreach($questions as $question) {
		// count the number of answers given to each question
		$statement = $db->prepare('SELECT count(question) FROM entry WHERE question = ?');
		$statement->execute(array($question['id']));
		$answers_amount = $statement->fetch()['count(question)'];
		echo '
			<tr>
				<td>' . $question['question_text'] . '</td>
				<td>';
		if ($answers_amount > 0)
		{
			echo '
				<a href="showanswers.php?taskid=' . $_GET['taskid'] . '&qid=' . $question['id'] . '">' . $answers_amount . '</a>';
		}
		else
		{
			echo $answers_amount;
		}
		echo '</td>
			</tr>';
	}
?>
					</table>
					<br>
					<a id="button" href="edittask.php?taskid=<?=$_GET['taskid']?>">Edit</a> 
					<a id="button" href="addquestion.php?taskid=<?=$_GET['taskid']?>">Add</a><br><br>
				</p>
			</div>
		</div>
	</body>
	
</html>