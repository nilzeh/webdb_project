<?php
function showRegisterPage()
{
	/* Shows a register page. */
	if (isset($_SESSION['message'])) {
		echo $_SESSION['message'] . '<br>';
	}
	// echo 'Register:';
	echo '
	<form action="register.php" method="post">
		<label for="email_address">Email address: </label>
		<input id="email_address" type="text" name="email_address"/>
		<br>
		<label for="nickname">Nickname: </label>
		<input id="nickname" type="text" name="nickname"/>
		<br>
		<label for="password">Password: </label>
		<input id="password" type="password" name="password"/>
		<br>
		<label for="gender">Gender: </label>
		<input type="radio" id="gender" name="gender" value="M"/> Male
		<input type="radio" id="gender" name="gender" value="F"/> Female
		<br>
		<label for="birthdate">Birthdate: </label>
		<input id="birthdate" type="date" name="birthdate"/>
		<br>
		<input type="submit" name="submit" value="submit"/>
	</form>';
}

function showLoginPage()
{
	/* Shows a login page. */
	session_start();
	if (isset($_SESSION['message'])) {
		echo $_SESSION['message'] . '<br>';
		unset($_SESSION['message']);
	}
	// echo 'Login:';
	echo '
	<form action="login.php" method="post">
		<label for="email_address">Email address: </label>
		<input id="email_address" type="text" name="email_address"/>
		<br>
		<label for="password">Password: </label>
		<input id="password" type="password" name="password"/>
		<br>
		<input type="submit" name="submit" value="submit"/>
	</form>
	Not a member yet? Click <a href="register.php">here</a> to register.';
}

function insertAccount($db)
{
	// Insert a new account in the DB
	// Create a random salt
	$random_salt = hash('sha512', uniqid(openssl_random_pseudo_bytes(16), TRUE));
	// Create hashed and salted password
	$password = hash('sha512', $_POST['password'] . $random_salt);

	//Insert into database
	$statement = $db->prepare('INSERT INTO account (email_address, password, salt, nickname, score, gender, birthdate) VALUES(?, ?, ?, ?, 0, ?, STR_TO_DATE(?, "%Y-%m-%d"))');
	$is_inserted = $statement->execute(array(
		strip_tags($_POST['email_address']), $password,
		$random_salt, strip_tags($_POST['nickname']),
		strip_tags($_POST['gender']), strip_tags($_POST['birthdate'])));
	//Return if insertion succeeded
	return $is_inserted;
}
?>