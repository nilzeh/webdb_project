<?php  		
	session_start();
	include 'functions.php';
	require_once('../db_config.php');
	$db = connectDB();
?>
<!DOCTYPE html>
<html>
<head>
	<title>
		Register
	</title>
	<link href="../stylesheet.css" rel="stylesheet" type="text/css">
</head>
<body>
	<div id="wrapper">
		<h1>
			Register
		</h1>
	<?php
		// Initialization
		include 'navbar.php';
		echo '<div id="content">';
		if (isset($_POST['submit'])) 
		{
			// Data filled in
			$is_inserted = insertAccount($db);
			if($is_inserted) 
			{
				// Account insertion went fine, redirect to login with message
				$_SESSION['message'] = 'User ' . $_POST['nickname'] . ' created!';
				header('Location: login.php', true);
				die();
			}
			echo 'Account registration failed, please try again.<br>';
			showRegisterPage();
		} 
		else 
		{
			showRegisterPage();
		}
	?>
		</div>
	</div>
</body>
</html>
