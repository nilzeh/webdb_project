<?php
	session_start();
	include 'functions.php';
	require_once('../db_config.php');
	// Initialize DB connection
	$db = connectDB();
?>
<html>
<head>
	<title>Login</title>
	<link href="../stylesheet.css" rel="stylesheet" type="text/css">
</head>
<body>
	<div id="wrapper">
		<h1>
			Login
		</h1>
	<?php
		include 'navbar.php';
		echo '<div id="content">';

		if (isset($_POST['submit'])) {
			// Credentials provided, check email_address/password combination
			// Get account from specific email_address
			$statement = $db->prepare("SELECT * FROM account WHERE email_address = ?");
			$statement->execute(array($_POST['email_address']));
			$account = $statement->fetch();
			//Hash provided password with unique salt from DB
			$password = hash('sha512', $_POST['password'] . $account['salt']);

			if($password == $account['password']) {
				// provided password is equal to stored password
				// Logged in correctly, Start user session and redirect to index
				session_destroy();
				session_start();
				$_SESSION['email_address'] = $_POST['email_address'];
				$_SESSION['timestamp'] = time();
				header('Location: ../leaderboards/index.php', true);
				die();
			} else {
				// Wrong credentials, give error
				echo 'Wrong email address or password. <br>';
				showLoginPage();
			}
		} else {
			// Show login page
			showLoginPage();
		}
	?>
		</div>
	</div>
</body>
</html>
