\contentsline {section}{\numberline {1}Introduction}{3}{section.1}
\contentsline {section}{\numberline {2}Requirements}{3}{section.2}
\contentsline {subsection}{\numberline {2.1}Client side}{3}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Server side}{3}{subsection.2.2}
\contentsline {section}{\numberline {3}Glossary}{3}{section.3}
\contentsline {subsection}{\numberline {3.1}Crowd Sourcing}{3}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Task}{3}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Owner}{3}{subsection.3.3}
\contentsline {subsection}{\numberline {3.4}User information}{4}{subsection.3.4}
\contentsline {section}{\numberline {4}Database Model}{4}{section.4}
\contentsline {subsection}{\numberline {4.1}\emph {Account}}{4}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}\emph {Task}}{5}{subsection.4.2}
